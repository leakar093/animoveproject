//
//  menuBarViewController.swift
//  AnimooveProject
//
//  Created by User on 15/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
protocol pushControllerDelegate {
    func pushControllerDelegate(view:UIViewController)
}
class menuBarViewController: UIViewController,pushControllerDelegate {
    
    
    func pushControllerDelegate(view:UIViewController) {
       
        Global.sharedInstance.mainPage?.navigationController?.pushViewController(view, animated: true)
    }
    
    
    var btnMenu : UIBarButtonItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance.menuBar = self
        let rightBarButton = UIButton(type: UIButtonType.custom) as UIButton
        //rightBarButton.setBackgroundImage(UIImage(named: "Ellipse 2 copy 2.png"), for: .normal)
        rightBarButton.setTitleColor(Colors.sharedInstance.whiteBackGround, for: .normal)
       rightBarButton.titleLabel?.font =  UIFont(name: "icomoon", size: 25)
         rightBarButton.setTitle("\u{e928}", for: .normal)
       
        rightBarButton.addTarget(self, action: #selector(menuBarViewController.openMenu), for: UIControlEvents.touchUpInside)

        let rbarButton = UIBarButtonItem(customView: rightBarButton)
        
        self.navigationItem.rightBarButtonItem = rbarButton
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var x = 0
    @objc func openMenu()
    {

         //   _ = DejalActivityView(forView: self.view, withLabel: "")
            // delegate!.presentMenuOnSelf()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pvc:menuViewController = storyboard.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
        pvc.modalPresentationStyle = UIModalPresentationStyle.custom
//            let v:UIView = pvc.view
//            var f:CGRect = v.frame
//            f.origin.x += self.view.frame.size.width //move to right
//
//            v.frame = f
//
//            UIView.animateWithDuration(1.5 ,animations:{
//                v.frame = self.view.frame;
//            } , completion: {finished in
        if Global.sharedInstance.isMenuOpen == false{
        self.present(pvc, animated: false, completion: nil)
            Global.sharedInstance.isMenuOpen = true
        }
        else{
            Global.sharedInstance.isMenuOpen = false
            self.dismiss(animated: false, completion: nil)
        }
         //  })

        //}
 
    }
    
    func presentMenuOnSelf(view: UIViewController)
    {
        self.present(view, animated: true, completion: nil)
    }
    func push(view: UIViewController)
    {
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
