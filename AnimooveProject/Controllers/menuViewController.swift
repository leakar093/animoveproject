//
//  menuViewController.swift
//  AnimooveProject
//
//  Created by User on 15/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var delegate:pushControllerDelegate!=nil
    @IBOutlet weak var tblMenu: UITableView!
    
    @IBOutlet weak var viMenu: UIView!
    var array:Array<String> = ["Favorites","Shop","Sent Animoove","Setting","Info","Register","Be AniMoover","Share AniMoove"]
    var arrayImages:Array<String> = ["\u{e925}","\u{e92a}","\u{e918}","\u{e92c}","\u{e92b}","\u{e917}","\u{e910}","\u{e921}"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   return  array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let  cell = tableView.dequeueReusableCell(withIdentifier: "itemWithImageTableViewCell")as!itemWithImageTableViewCell
        cell.setDisplayData(st: array[indexPath.row],stImg: arrayImages[indexPath.row])
        if indexPath.row > 5{
            cell.lblImg.textColor = Colors.sharedInstance.pinkColor
            cell.lblDesc.textColor = Colors.sharedInstance.pinkColor
            cell.lblDesc.font = UIFont(name: cell.lblDesc.font.fontName, size: 16)
        }
        if indexPath.row == 5{
            cell.viButtom.isHidden = false
        }
        else{
             cell.viButtom.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let Favorites:FavoritesViewController = self.storyboard?.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
            self.dismiss(animated: false, completion: nil)
            Global.sharedInstance.isMenuOpen = false
            delegate.pushControllerDelegate(view: Favorites)
        break
        case 3:
            let setting:SettingViewController = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
            self.dismiss(animated: false, completion: nil)
         Global.sharedInstance.isMenuOpen = false
            delegate.pushControllerDelegate(view: setting)
            break
        default:
            break
        }

      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tblMenu.frame.height /
            CGFloat(array.count)
        //    return self.tblVideos.bounds.height / 1.5
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
tblMenu.separatorStyle = .none
        self.delegate = Global.sharedInstance.menuBar
      
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
          viMenu.dropShadow()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        let bounds = UIScreen.main.bounds
        
        let width = bounds.size.width
        let height = bounds.size.height
        let y = 75
        //width: width*0.8
        self.view.superview!.bounds = CGRect(x:-(width/6.5),y:CGFloat(y), width: width*0.55,height: height*0.6)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
