//
//  SortAnimooveByImugiesViewController.swift
//  AnimooveProject
//
//  Created by User on 30/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class SortAnimooveByImugiesViewController: NavigationModelViewController,UITableViewDataSource,UITableViewDelegate,presentDetailsDelegate,showAlertDelegate,openTblContainerDelegate,reloadSearchTableDelegate{
    
    
   
    
    @IBOutlet weak var conTblSearchHeight: NSLayoutConstraint!
    var generic:Generic = Generic()
    func reloadSearchTable(filtered: Array<Category>) {
        Global.sharedInstance.filteredSearch = filtered
        if filtered.count > 3{
            conTblSearchHeight.constant =  self.view.bounds.height * 0.3
        }
        else{
            conTblSearchHeight.constant = CGFloat(Global.sharedInstance.filteredSearch.count) *  (self.view.bounds.height * 0.07)
        }
        if filtered.count == 0{
            conTblSearchHeight.constant = 0
        }
    }
    
    
    
    
    func openTblContainer(status:Int,fromTap:Int) {
        
        if status == 1 && fromTap != 2{
            conHeightTblContainer.constant = self.view.bounds.height * 0.25
        }
        else if fromTap == 2 && status == 1{
            conHeightTblContainer.constant = CGFloat(Global.sharedInstance.keyboardEight)
        }
        else{
            conHeightTblContainer.constant = 0
            conTblSearchHeight.constant = 0
        }
        
    }
   
    
    
    func showAlert() {
        Alert.sharedInstance.showAlerMess(mess: "הבחירה לא נשמרה במערכת ", vc: self)
    }
    
    @IBOutlet weak var barContainer: UIView!
    @IBOutlet weak var conHeightTblContainer: NSLayoutConstraint!
    @IBOutlet weak var viScraller: UIView!
    
    @IBOutlet weak var viScrallerHeightCon: NSLayoutConstraint!
    func presentDetails(ani: Animoove) {
        let viewCon = storyboard?.instantiateViewController(withIdentifier: "DetailsAnimooveViewController")as!DetailsAnimooveViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.custom
        
        viewCon.ani = ani
        self.navigationController?.present(viewCon, animated: true, completion: nil)
    }
    
    
    
  
    
    
    @IBOutlet weak var tblVideos: UITableView!
    
    @IBOutlet weak var scraller: UIScrollView!
    @IBOutlet weak var collectionCategories: UICollectionView!
    
    @IBOutlet weak var collAnimoovies: UICollectionView!
    
    @IBOutlet weak var heightConTblVideos: NSLayoutConstraint!
    @IBOutlet weak var tblAnimooveImugies: UITableView!
    // @IBOutlet weak var tblVideos: UITableView!
    var arrCategories:Array<Category> = []
    //var arrAnimoovies:Array<Animoove> = []
    var heightTbl:CGFloat = 0
    var category:Category?
    override func viewDidLoad() {
        super.viewDidLoad()
        let dic = Global.sharedInstance.defaults.dictionary(forKey: "oUser")
        var user = User().getUserFromDic(dic: dic as! Dictionary<String, AnyObject>)
        Global.sharedInstance.userRegister = user
        tblAnimooveImugies.separatorStyle = .none
       // Global.sharedInstance.mainPage = self
     
 self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        // print(heightConTblVideos.constant)
        //        tblVideos.reloadData()
        heightTbl =  self.tblAnimooveImugies.bounds.height/1.1
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if (category != nil){
            self.navigationItem.title = category?.nvName
            self.navigationItem.titleView = nil
        }
            self.navigationItem.setHidesBackButton(true, animated:true)
    }
    override func viewDidLayoutSubviews() {
        
        //  scraller.isScrollEnabled = true
        // Do any additional setup after loading the view
        //  scraller.contentSize = CGSize(width: 200, height: heightConTblVideos.constant + collectionCategories.bounds.height)
        
        //        viScrallerHeightCon.constant =  heightConTblVideos.constant
        //        viScraller.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return  Global.sharedInstance.arrayAnimooveByImugies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let  cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell")as!VideoTableViewCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.delegateAlert = self
        if  Global.sharedInstance.arrayAnimooveByImugies.count > 0
        {cell.setDisplayData(ani:  Global.sharedInstance.arrayAnimooveByImugies[indexPath.row],index: indexPath.row, isVisible: true)}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("asf")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        return tblAnimooveImugies.bounds.size.height /
        2
        //    return self.tblVideos.bounds.height / 1.5
        
    }
    
  
    
    
    
    func UpdateUserAnimovie()
    {
        let dic = Dictionary<String,AnyObject>()
        
        //        dic[“iUserId”] = 1 as AnyObject
        //        dic[“iContractReportId”] = 31 as AnyObject
        
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        
                    }
                    else{
                        print(dic["Error"])
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.UpdateUserAnimovie)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ModelContainerViewController {
            (segue.destination as! ModelContainerViewController).delegate = self
            (segue.destination as! ModelContainerViewController).delegateSearch = self
            //            (segue.destination as! TravelsViewController).getTravels()
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     Get the new view controller using segue.destinationViewController.
     Pass the selected object to the new view controller.
     }
     */
    
}

