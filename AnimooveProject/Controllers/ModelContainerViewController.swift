//
//  ModelContainerViewController.swift
//  AnimooveProject
//
//  Created by User on 19/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
protocol openTblContainerDelegate {
    func openTblContainer(status:Int,fromTap:Int)
}
protocol reloadSearchTableDelegate {
    func reloadSearchTable(filtered:Array<Category>)
}
class ModelContainerViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var txtSearchOpen: UITextField!
    @IBOutlet weak var btnTags: UIButton!
    @IBOutlet weak var btnSmile: UIButton!
    var filtered:Array<Category> = []
    @IBOutlet weak var viSearchOpen: UIView!
    @IBOutlet weak var txtSearch: UITextField!
    var delegate:openTblContainerDelegate!=nil
    @IBOutlet weak var viTextSearch: UIView!
    @IBOutlet weak var btnVoice: UIButton!
    var delegateSearch:reloadSearchTableDelegate!=nil
    override func viewDidLoad() {
        super.viewDidLoad()
   
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ModelContainerViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        btnSmile.setTitle("\u{e91b}", for: .normal)
        
        btnTags.setTitle("\u{e91a}", for: .normal)
        btnVoice.setTitle("\u{e919}", for: .normal)
        txtSearch.delegate = self
        txtSearchOpen.delegate = self
        txtSearchOpen.addTarget(self, action: #selector(ModelContainerViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        txtSearch.addTarget(self, action: #selector(ModelContainerViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        z = 0//סוגר את המקלדת ומוריד את הקונטינר
     delegate.openTblContainer(status: z,fromTap:2)
        viTextSearch.isHidden = false
        viSearchOpen.isHidden = true
    }
    @objc private func keyboardWillShow(notification:NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        Global.sharedInstance.keyboardEight = Int(keyboardHeight)
          delegate.openTblContainer(status: z,fromTap:2)//from smile
        viTextSearch.isHidden = true
        txtSearchOpen.becomeFirstResponder()
//        editorBottomCT.constant = keyboardHeight
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        viTextSearch.layer.cornerRadius = 15
        viTextSearch.layer.borderColor = Colors.sharedInstance.grayColor.cgColor
         viTextSearch.layer.borderWidth = 1
        viSearchOpen.layer.cornerRadius = 15
        viSearchOpen.layer.borderColor = Colors.sharedInstance.grayColor.cgColor
        viSearchOpen.layer.borderWidth = 1
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var x = 0
    @IBAction func openSmile(_ sender: UIButton) {
        if x == 0{
            x = 1//open
            Global.sharedInstance.tagOrImugi = 0
            btnSmile.setTitleColor(Colors.sharedInstance.blueKeybourd, for: .normal)
        }else{
            x = 0//close
            Global.sharedInstance.tagOrImugi = -1
btnSmile.setTitleColor(Colors.sharedInstance.grayKeyboard, for: .normal)
        }
        
        delegate.openTblContainer(status: x,fromTap:0)//from smile
    }
    var y = 0
    @IBAction func openTags(_ sender: UIButton) {
        
        if y == 0{
            y = 1//open
            btnTags.setTitleColor(Colors.sharedInstance.blueKeybourd, for: .normal)
            Global.sharedInstance.tagOrImugi = 1
        }else{
            y = 0//close
            btnTags.setTitleColor(Colors.sharedInstance.grayKeyboard, for: .normal)
            
            Global.sharedInstance.tagOrImugi = -1
        }
        
        delegate.openTblContainer(status: y,fromTap:1)//from tags
    }
    var z = 0
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //if z == 0{
        viSearchOpen.isHidden = false
              z = 1//פותח את המקלדת ומעלה את הקונטינר
//txtSearchOpen.becomeFirstResponder()
    }
    
    func searching(_ searchText: String) {
        //let s:CGFloat = view.frame.size.height - tableView.frame.origin.x
        
        if searchText == ""
        {
            filtered = Global.sharedInstance.arrCategories
        }
        else
        {
            
           
            
filtered =  Global.sharedInstance.arrCategories.filter({ (category) ->  Bool in
                //   let tmp: NSString = vertex.description[0]
    var tmp: NSString = category.nvName as NSString
                
                //מתחיל ולא מכיל
                                if tmp.description.characters.count >= searchText.characters.count
                                {
                                    tmp = tmp.substring(to: searchText.characters.count) as NSString
                                }
    
                let range = tmp.range(of: searchText,options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
            
            
            
        }
        if(filtered.count == 0){
           // self.tblSearch.isHidden = true
           
            //filtered.sort { $0.nvName.compare($1.nvName) == .orderedAscending }
            //   filtered.sortInPlace { $0.description[0].compare($1.description[0]) == .OrderedAscending }
        }
//        else{
//            self.tblSearch.isHidden = false
//            self.viMain.isHidden = false
//        }
        //    tblSearch.reloadData()
      //  uploadTable()
        delegateSearch.reloadSearchTable(filtered: filtered)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if txtSearchOpen.text != ""{
            searching(txtSearchOpen.text!)
    }
        else{
            filtered = []
             delegateSearch.reloadSearchTable(filtered: filtered)
        }
    }
    
//    func uploadTable()
//    {
//        if filtered.count < 5
//        {
//            conTableHeight.constant = (CGFloat)((self.view.bounds.height * 0.07) * CGFloat( filtered.count))
//            conHieghtViMain.constant = conTableHeight.constant
//        }
//
//        else {
//            conTableHeight.constant = (CGFloat)((self.view.bounds.height * 0.07) * 5)
//            conHieghtViMain.constant = conTableHeight.constant
//        }
//        tblSearch.layoutIfNeeded()
//        self.tblSearch.reloadData()
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
