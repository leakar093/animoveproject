//
//  HomeViewController.swift
//  AnimooveProject
//
//  Created by User on 07/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit


class HomeViewController: NavigationModelViewController,UITableViewDataSource,UITableViewDelegate,presentDetailsDelegate,showAlertDelegate,reloadTblMooviesDelegate,openTblContainerDelegate,reloadSearchTableDelegate,controllerToPushDelegate{
    func controllerToPush(view: UIViewController, cat: Category, FromKey: Int) {
        
      (view as! SortAnimooveByImugiesViewController).category = cat
       self.navigationItem.title = cat.nvName
    navigationController?.pushViewController(view, animated: true)
    }
    
   var visibleIP : IndexPath?
    @IBOutlet weak var conTblSearchHeight: NSLayoutConstraint!
    var generic:Generic = Generic()
    var isScrolling:Bool = false
    var index:Int = 0
    func reloadSearchTable(filtered: Array<Category>) {
        Global.sharedInstance.filteredSearch = filtered
        if filtered.count > 3{
          conTblSearchHeight.constant =  self.view.bounds.height * 0.3
        }
        else{
            conTblSearchHeight.constant = CGFloat(Global.sharedInstance.filteredSearch.count) *  (self.view.bounds.height * 0.07)
        }
        if filtered.count == 0{
            conTblSearchHeight.constant = 0
        }
    }
    
    
    var ifReloadCategory:Bool = false
    
    func openTblContainer(status:Int,fromTap:Int) {

        if status == 1 && fromTap != 2{
        conHeightTblContainer.constant = self.view.bounds.height * 0.25
        }
        else if fromTap == 2 && status == 1{
            conHeightTblContainer.constant = CGFloat(Global.sharedInstance.keyboardEight)
        }
        else{
            conHeightTblContainer.constant = 0
            conTblSearchHeight.constant = 0
        }
        
    }
    func reloadTbl(categoryID:Int) {
       // tblMoovies.reloadData()
       self.generic.showNativeActivityIndicator(cont: self)
      getMooviesByCategory(categoryID: categoryID)
    }
    
    
    func showAlert() {
        Alert.sharedInstance.showAlerMess(mess: "הבחירה לא נשמרה במערכת ", vc: self)
    }
      var aboutToBecomeInvisibleCell = -1
    @IBOutlet weak var barContainer: UIView!
    @IBOutlet weak var conHeightTblContainer: NSLayoutConstraint!
    @IBOutlet weak var viScraller: UIView!
    
    @IBOutlet weak var viScrallerHeightCon: NSLayoutConstraint!
    func presentDetails(ani: Animoove) {
        let viewCon = storyboard?.instantiateViewController(withIdentifier: "DetailsAnimooveViewController")as!DetailsAnimooveViewController
        viewCon.modalPresentationStyle = UIModalPresentationStyle.custom
        
        viewCon.ani = ani
        self.navigationController?.present(viewCon, animated: true, completion: nil)
    }
    
   
    
    @IBOutlet weak var tblMoovies: UITableView!
    
    
    @IBOutlet weak var tblVideos: UITableView!
    
    @IBOutlet weak var scraller: UIScrollView!
    @IBOutlet weak var collectionCategories: UICollectionView!
    
    @IBOutlet weak var collAnimoovies: UICollectionView!
    var fullvisible = false
    @IBOutlet weak var heightConTblVideos: NSLayoutConstraint!
   // @IBOutlet weak var tblVideos: UITableView!
    var arrCategories:Array<Category> = []
    //var arrAnimoovies:Array<Animoove> = []
    var heightTbl:CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let dic = Global.sharedInstance.defaults.dictionary(forKey: "oUser")
        var user = User().getUserFromDic(dic: dic as! Dictionary<String, AnyObject>)
        Global.sharedInstance.userRegister = user
      tblMoovies.separatorStyle = .none
        Global.sharedInstance.mainPage = self
         getCategories()
        visibleIP = IndexPath.init(row: 0, section: 0)

    }
    
    
        
  
   
    override func viewDidAppear(_ animated: Bool) {
       // print(heightConTblVideos.constant)
//        tblVideos.reloadData()
   heightTbl =  self.tblMoovies.bounds.height/1.1
    }
   
     override func viewWillAppear(_ animated: Bool) {
        if Global.sharedInstance.defaults.value(forKey: "isAutoPlay") != nil{
    Global.sharedInstance.isAutoPlay = Global.sharedInstance.defaults.value(forKey: "isAutoPlay")as! Bool
        }
            if Global.sharedInstance.defaults.value(forKey: "isLoop") != nil{
                Global.sharedInstance.isLoop = Global.sharedInstance.defaults.value(forKey: "isLoop")as! Bool
        }
self.navigationController?.setNavigationBarHidden(false, animated: animated)
        tblMoovies.reloadData()
    }
    override func viewDidLayoutSubviews() {
       
      //  scraller.isScrollEnabled = true
        // Do any additional setup after loading the view
      //  scraller.contentSize = CGSize(width: 200, height: heightConTblVideos.constant + collectionCategories.bounds.height)

//        viScrallerHeightCon.constant =  heightConTblVideos.constant
//        viScraller.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: - tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return  Global.sharedInstance.arrayAnimooveByCategory.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let  cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesTableViewCell")as!CategoriesTableViewCell
            cell.delegate = self
            cell.selectionStyle = .none
            if Global.sharedInstance.ifReloadCategory == false{
       cell.setDisplayData()

            }
 
            return cell
        }
          let  cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell")as!VideoTableViewCell
        cell.selectionStyle = .none
        
       
        cell.delegate = self
        cell.delegateAlert = self
//        let cellRect = tableView.rectForRow(at: indexPath)
        //let completelyVisible = tableView.bounds.contains(cellRect)
       var completelyVisible = ifCellVisible(cell: cell,index: indexPath)
    // let completelyVisible = tableView.bounds.contains(tableView.rectForRow(at: indexPath))
if  Global.sharedInstance.arrayAnimooveByCategory.count > 0 
        {
            
            cell.setDisplayData(ani:  Global.sharedInstance.arrayAnimooveByCategory[indexPath.row],index: indexPath.row, isVisible: completelyVisible)}
      cell.index = indexPath
        return cell
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("asf")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return self.view.bounds.height * 0.14
        }
 return tblMoovies.bounds.size.height /
           2
    //    return self.tblVideos.bounds.height / 1.5
        
    }
    func checkVisibilityOf(_ cell: VideoTableViewCell, in aScrollView: UIScrollView) {
        let cellRect: CGRect = aScrollView.convert(cell.frame, to: aScrollView.superview)
        if aScrollView.frame.contains(cellRect){
            //cell.notifyCompletelyVisible()
        }
        else {
            //cell.notifyNotCompletelyVisible()
        }
    }
    func checkVisibilityOfCat(_ cell: CategoriesTableViewCell, in aScrollView: UIScrollView) {
        let cellRect: CGRect = aScrollView.convert(cell.frame, to: aScrollView.superview)
        if aScrollView.frame.contains(cellRect) {
            //cell.notifyCompletelyVisible()
        }
        else {
            //cell.notifyNotCompletelyVisible()
        }
    }
    //  Converted to Swift 4 with Swiftify v1.0.6536 - https://objectivec2swift.com/
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var cells = tblMoovies.visibleCells
        let cellCount: Int = cells.count
        //cells.remove(at: 0)
        if cellCount == 0 {
            return
        }
        // Check the visibility of the first cell
        if (cells.first is VideoTableViewCell){
        checkVisibilityOf(cells.first as! VideoTableViewCell, in: scrollView)
        }
        else{
              checkVisibilityOfCat(cells.first as! CategoriesTableViewCell, in: scrollView)
        }
        if cellCount == 1 {
            return
        }
        // Check the visibility of the last cell
        checkVisibilityOf(cells.last as! VideoTableViewCell, in: scrollView)
        if cellCount == 2 {
            return
        }
        // All of the rest of the cells are visible: Loop through the 2nd through n-1 cells
        for i in 1..<cellCount - 1 {
            //cells[i].notifyCompletelyVisible()
        }
    }
    //  Converted to Swift 4 with Swiftify v1.0.6536 - https://objectivec2swift.com/
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
//        var cells:Array<VideoTableViewCell> = tblMoovies.visibleCells as! Array<VideoTableViewCell>
//        let offset = scrollView.contentOffset
//        let bounds = scrollView.bounds
//        cells.remove(at: 0)
//        for cell in cells {
//            if cell.frame.origin.y > offset.y && cell.frame.origin.y + cell.frame.size.height < offset.y + bounds.size.height {
//            var path: IndexPath? = tblMoovies.indexPath(for: cell)
//                index = (path?.row)!
//                fullvisible = true
//                tblMoovies.reloadData()
//            }
//            else {
//                fullvisible = false
//            }
//        }
//    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
      //  isScrolling = false
   //tblMoovies.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
       // isScrolling = true
      //tblMoovies.reloadData()
     //   index = -1
    }
    
    
    

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let indexPaths = self.tblMoovies.indexPathsForVisibleRows
//        var cells = [Any]()
//        for ip in indexPaths!{
//            if let videoCell = self.tblMoovies.cellForRow(at: ip) as? VideoTableViewCell{
//                cells.append(videoCell)
//            }
//        }
//        let cellCount = cells.count
//        if cellCount == 0 {return}//אם אין גלוים
//        if cellCount == 1{//אם יש גלוי אחד
//            if visibleIP != indexPaths?[0]{//מאתחל את האינדקס הנוכחי הגלוי
//                visibleIP = indexPaths?[0]
//            }
//            if let videoCell = cells.last! as? VideoTableViewCell{
////                self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?.last)!)
//                videoCell.isVisibleToPlay = true
//                //tblMoovies.reloadRows(at: [(indexPaths?.last)!], with: .automatic)
//            }
//        }
//        if cellCount >= 2 {
//            for i in 0..<cellCount{
//                let cellRect = self.tblMoovies.rectForRow(at: (indexPaths?[i])!)
//                let intersect = cellRect.intersection(self.tblMoovies.bounds)
//                //                curerntHeight is the height of the cell that
//                //                is visible
//                let currentHeight = intersect.height
//                print("\n \(currentHeight)")
//                let cellHeight = (cells[i] as AnyObject).frame.size.height
//                //                0.95 here denotes how much you want the cell to display
//                //                for it to mark itself as visible,
//                //                .95 denotes 95 percent,
//                //                you can change the values accordingly
//                if currentHeight > (cellHeight * 0.95){
//                    if visibleIP != indexPaths?[i]{
//                        visibleIP = indexPaths?[i]
//                        print ("visible = \(indexPaths?[i])")
//                        if let videoCell = cells[i] as? VideoTableViewCell{
//                            //self.playVideoOnTheCell(cell: videoCell, indexPath: (indexPaths?[i])!)
//                       videoCell.isVisibleToPlay = true
//                      //  tblMoovies.reloadRows(at: [indexPaths![i]], with: .automatic)
//
//                        }
//                    }
//                }
//                else{
//                    if aboutToBecomeInvisibleCell != indexPaths?[i].row{
//                        aboutToBecomeInvisibleCell = (indexPaths?[i].row)!
//                        if let videoCell = cells[i] as? VideoTableViewCell{
//                           // self.stopPlayBack(cell: videoCell, indexPath: (indexPaths?[i])!)
//                            videoCell.isVisibleToPlay = false
//                            //tblMoovies.reloadRows(at: [indexPaths![i]], with: .automatic)
//                        }
//
//                    }
//                }
//            }
//        }
//    }
    
    func ifCellVisible(cell:VideoTableViewCell,index:IndexPath) -> Bool {
        let cellRect = self.tblMoovies.rectForRow(at: (index))
        let intersect = cellRect.intersection(self.tblMoovies.bounds)
        //                curerntHeight is the height of the cell that
        //                is visible
        let currentHeight = intersect.height
        print("\n \(currentHeight)")
        let cellHeight = (cell as AnyObject).frame.size.height
        //                0.95 here denotes how much you want the cell to display
        //                for it to mark itself as visible,
        //                .95 denotes 95 percent,
        //                you can change the values accordingly
        if currentHeight > (cellHeight * 0.95){
            if visibleIP != index{
                visibleIP = index
                print ("visible = \(index)")
                return true
            }
        }
        return false
    }
    
    func playVideoOnTheCell(cell : VideoTableViewCell, indexPath : IndexPath){
        //cell.startPlayback()
//        cell.isVisibleToPlay = true
//        tblMoovies.reloadRows(at: [indexPath], with: .none)
    }
    func stopPlayBack(cell : VideoTableViewCell, indexPath : IndexPath){
       // cell.stopPlayback()
//        cell.isVisibleToPlay = false
//        tblMoovies.reloadRows(at: [indexPath], with: .none)
    }
//    func scrollViewDidScroll(_ aScrollView: UIScrollView) {
//        let cells:Array<VideoTableViewCell> = tblMoovies.visibleCells as! Array<VideoTableViewCell>
//        var offset: CGPoint = aScrollView.contentOffset
//        var bounds: CGRect = aScrollView.bounds
//        for cell: VideoTableViewCell in cells {
//            if cell.frame.origin.y > offset.y && cell.frame.origin.y + cell.frame.size.height < offset.y + bounds.size.height {
//                let path: IndexPath? = tblMoovies.indexPath(for: cell as? UICollectionViewCell ? UICollectionViewCell())
//                index = (path?.row)!
//                fullvisible = true
//                homeTabl.reloadData()
//            }
//            else {
//                fullvisible = false
//            }
//        }
//    }

    func getVisibleIndex() -> Int {
        for indexPath: IndexPath in tblMoovies.indexPathsForVisibleRows! {
            let cellRect: CGRect = tblMoovies.rectForRow(at: indexPath)
            let isVisible: Bool = tblMoovies.bounds.contains(cellRect)
            if isVisible {
                index = Int(indexPath.row)
            }
        }
        return index
    }
    
    
 
    
    func updateTblHeight(){
       heightConTblVideos.constant = ((heightTbl) * CGFloat( Global.sharedInstance.arrayAnimooveByCategory.count))
        //viScrallerHeightCon.constant =  heightConTblVideos.constant
        //viScraller.layoutIfNeeded()
        tblVideos.updateConstraints()
        tblVideos.reloadData()
          scraller.contentSize = CGSize(width: 200, height: heightConTblVideos.constant + collectionCategories.bounds.height)
       
       //viScrallerHeightCon.constant = scraller.contentSize.height
           //viScraller.frame.size.height = 10000
    }
   
  // MARK: - api
    func getCategories()
    {
        let dic = Dictionary<String,AnyObject>()
        
//        dic[“iUserId”] = 1 as AnyObject
//        dic[“iContractReportId”] = 31 as AnyObject
        
       generic.showNativeActivityIndicator(cont: self)
       api.sharedInstance.goServer(params: dic, success: {
        (param1, response) -> Void in
        
    if let result = response as? Dictionary<String, AnyObject>{
        var dic:Dictionary<String, AnyObject> = result
        
        if let error = dic["Error"] as? NSDictionary
        {
            if error["iErrorCode"] as? Int == 0 {
               Global.sharedInstance.arrCategories = Category().categoriesToArrayGet(arrDic: dic["Result"] as! Array<Dictionary<String,AnyObject>>)
                
                
            //  self.tblMoovies.reloadData()
            Global.sharedInstance.loadImugies()
                self.getMooviesByCategory(categoryID: 2)
            }
            else{
                print(dic["Error"])
            }
        }
        
        }
        
        }, failure: {
        (dataTask, error) -> Void in
        
        print(error.localizedDescription)
        
        }, funcName: api.sharedInstance.GetCategoriesList)
    }
    
    
    func getMooviesByCategory(categoryID:Int)
    {
        var dic = Dictionary<String,AnyObject>()
        dic["iUserId"] = Global.sharedInstance.userRegister.iUserId as AnyObject
        dic["iCategoryId"] = categoryID as AnyObject
        
        //generic.showNativeActivityIndicator(cont: Global.sharedInstance.mainPage!)

        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
              
                        
             Global.sharedInstance.arrayAnimooveByCategory = Animoove().animooveToArrayGet(arrDic: dic["Result"] as! Array<Dictionary<String,AnyObject>>)
                 //   self.updateTblHeight()
                    Global.sharedInstance.arrayAnimooveByCategory.remove(at: 1)
                    self.tblMoovies.reloadData()
                        self.generic.hideNativeActivityIndicator(cont: self)
                    }
                    else{
                        print(dic["Error"])
                        self.generic.hideNativeActivityIndicator(cont: self)
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            self.generic.hideNativeActivityIndicator(cont: self)
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetAnimoovesListByCategory)
    }
    
    func UpdateUserAnimovie()
    {
        let dic = Dictionary<String,AnyObject>()
        
        //        dic[“iUserId”] = 1 as AnyObject
        //        dic[“iContractReportId”] = 31 as AnyObject
        
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
          
                    }
                    else{
                        print(dic["Error"])
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.UpdateUserAnimovie)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ModelContainerViewController {
            (segue.destination as! ModelContainerViewController).delegate = self
             (segue.destination as! ModelContainerViewController).delegateSearch = self
            //            (segue.destination as! TravelsViewController).getTravels()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         Get the new view controller using segue.destinationViewController.
         Pass the selected object to the new view controller.
    }
    */

}
