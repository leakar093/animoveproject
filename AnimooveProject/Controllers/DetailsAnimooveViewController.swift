//
//  DetailsAnimooveViewController.swift
//  AnimooveProject
//
//  Created by User on 19/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
import AVKit
import Foundation
class DetailsAnimooveViewController: UIViewController {

    @IBOutlet weak var viShare: UIView!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var lblNumViews: UILabel!
    @IBOutlet weak var viExt: UIView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblNameAnimator: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var viAnimoove: UIView!
    @IBOutlet weak var imgAnimator: UIImageView!
    @IBOutlet weak var pngArrow: UIImageView!
    @IBOutlet weak var viDetailsAnimator: UIView!
    var ani:Animoove = Animoove()
    
    @IBOutlet weak var btnFB: UIButton!
    @IBOutlet weak var btnWA: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    
    @IBOutlet weak var btnSms: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DetailsAnimooveViewController.hideSelf))
        // tap.delegate = self
        self.view.addGestureRecognizer(tap)
          btnStar.setTitle("\u{e926}", for: .normal)
        viDetailsAnimator.layer.cornerRadius = viDetailsAnimator.bounds.height/2
        viDetailsAnimator.layer.masksToBounds = true
        
        imgAnimator.layer.cornerRadius = imgAnimator.bounds.height/2
        imgAnimator.clipsToBounds = true
        lblNumViews.text = String(ani.iViews) + "Views"
        lblNameAnimator.text = ani.nvAnimatorName
        btnLike.setTitle("\u{e927}", for: .normal)
        viAnimoove.layer.cornerRadius = 15
        viExt.layer.cornerRadius = 15
        //
        // btnShare.layer.cornerRadius = 10
          btnWA.setTitle("\u{e905}", for: .normal)
         btnFB.setTitle("\u{e908}", for: .normal)
         btnMail.setTitle("\u{e906}", for: .normal)
         btnSms.setTitle("\u{e911}", for: .normal)
         playVideo(from: ani.nvURL)
        // Do any additional setup after loading the view.
    }
 
    override func viewDidAppear(_ animated: Bool) {
        btnShare.layer.cornerRadius = btnShare.bounds.size.height/2
viShare.layer.cornerRadius = viShare.bounds.size.height/2
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func Share(_ sender: UIButton) {
        if viShare.tag == 0{
            viShare.isHidden = false
            pngArrow.isHidden = false
            viShare.tag = 1
        }
        else{
            viShare.isHidden = true
            pngArrow.isHidden = true

             viShare.tag = 0
        }
        
    }
    @objc func hideSelf(){
        self.dismiss(animated: true, completion: nil)
    }
    private func playVideo(from file:String) {
        
        let videoURL = URL(string: file)
        
        let player =  AVPlayer(url: videoURL!)
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.viAnimoove.layer.bounds
        
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        self.viAnimoove.layer.addSublayer(playerLayer)
        playerLayer.frame = self.viAnimoove.layer.bounds
        viAnimoove.clipsToBounds = true
        player.play()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
