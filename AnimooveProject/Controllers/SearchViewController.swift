//
//  SearchViewController.swift
//  AnimooveProject
//
//  Created by User on 26/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var viSearch: UIView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return Global.sharedInstance.filteredSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "searchTableViewCell")as!searchTableViewCell
      
        cell.selectionStyle = .none
 cell.setDisplayData(cat:Global.sharedInstance.filteredSearch[indexPath.row] )

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        return TblSearch.bounds.size.height / CGFloat(Global.sharedInstance.filteredSearch.count)
        return Global.sharedInstance.mainPage!.view.bounds.height * 0.07
        
    }

    @IBOutlet weak var TblSearch: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
      TblSearch.separatorStyle = .none
//        viSearch.layer.shadowColor = UIColor.black.cgColor
//       // viSearch.layer.shadowOffset = CGSize(width: 3, height: 3)
//
////        viSearch.layer.shadowOpacity = 5
//        viSearch.layer.shadowOpacity = 1.0
//        viSearch.layer.shadowRadius = 2
        viSearch.layer.borderColor = Colors.sharedInstance.grayKeyboard.cgColor
        viSearch.layer.borderWidth = 1
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        TblSearch.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
