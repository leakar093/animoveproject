//
//  ValidPhoneViewController.swift
//  AnimooveProject
//
//  Created by User on 09/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class ValidPhoneViewController: UIViewController {
    var con:SmsEnterViewController!
    @IBOutlet weak var txtPhone: UITextField!
    var generic:Generic = Generic()
    override func viewDidLoad() {
        super.viewDidLoad()
con = storyboard?.instantiateViewController(withIdentifier: "SmsEnterViewController") as!SmsEnterViewController
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ValidPhoneViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtPhone.text = ""
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
 var x = 0
    
    @IBAction func btnContinue(_ sender: UIButton) {
        if x == 0{
            x = 1
            validPhoneNumer()

        }
        x = 1
        
        
    }
    
    
    // MARK: - api
    func validPhoneNumer()
    {if txtPhone.text != ""{
        var dic = Dictionary<String,AnyObject>()
        
        dic["nvPhone"] = txtPhone.text as AnyObject
        //        dic[“iContractReportId”] = 31 as AnyObject
        generic.showNativeActivityIndicator(cont: self)
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                   var r = dic["Result"] as! Array<String>
                        Global.sharedInstance.smsKod =  Int(r[0])!
                        print(Global.sharedInstance.smsKod)
       Global.sharedInstance.phone = self.txtPhone.text!
                        self.generic.hideNativeActivityIndicator(cont: self)
 self.navigationController?.pushViewController(self.con, animated: true)
                    }
                    else{
                  if error["iErrorCode"] as? Int == 2601{
                        print(dic["Error"])
                     self.generic.hideNativeActivityIndicator(cont: self)
                    Alert.sharedInstance.showAlerMess(mess: "המס' קים במערכת", vc: self)
                        }
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
             self.generic.hideNativeActivityIndicator(cont: self)
        }, funcName: api.sharedInstance.ValidateVerificationCode)
    }
    else{
        Alert.sharedInstance.showAlerMess(mess: "חובה להכניס מס' טלפון", vc: self)
        }
    }
    
    func isValidPhoneNumber(str:String) -> Bool{
        if str.characters.count < 9
     {
    
            return false
        }
        return true
    }
    
    func isStringContainsOnlyNumbers(string: String) -> Bool {
        return CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: string))

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
