//
//  SettingViewController.swift
//  AnimooveProject
//
//  Created by User on 03/12/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class SettingViewController: NavigationModelViewController {

    @IBOutlet weak var swNotification: UISwitch!
    @IBOutlet weak var swLoop: UISwitch!
    @IBOutlet weak var swAutoPlay: UISwitch!
    @IBOutlet weak var lblArrow: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
lblArrow.text = "\u{e913}"
      //self.navigationController?.navigationBar.backItem?.title = "\u{e914}"
   SetBackBarButtonCustom()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         self.navigationItem.title = "Setting"
        self.navigationItem.titleView = nil
        swAutoPlay.isOn = Global.sharedInstance.isAutoPlay
          swLoop.isOn = Global.sharedInstance.isLoop
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func SetBackBarButtonCustom()
    {
        //Back buttion
        let btnLeftMenu: UIButton = UIButton()
       // btnLeftMenu.setImage(UIImage(named: "back_arrow"), for: UIControlState())
        btnLeftMenu.setTitle("\u{e914}", for: .normal)
        btnLeftMenu.titleLabel?.font =  UIFont(name: "icomoon", size: 20)
        btnLeftMenu.addTarget(self, action: #selector(self.onClcikBack), for: UIControlEvents.touchUpInside)
        btnLeftMenu.frame = CGRect(x: 0, y: 0, width: 33/2, height: 27/2)
        let barButton = UIBarButtonItem(customView: btnLeftMenu)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func onClcikBack()
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func swAutoPlayChange(_ sender: UISwitch) {
        if swAutoPlay.isOn {
            Global.sharedInstance.isAutoPlay = true
        } else {
             Global.sharedInstance.isAutoPlay = false
        }
        Global.sharedInstance.defaults.set(Global.sharedInstance.isAutoPlay, forKey: "isAutoPlay")
    }
    @IBAction func swLoopChange(_ sender: UISwitch) {
        if swLoop.isOn {
            Global.sharedInstance.isLoop = true
        } else {
            Global.sharedInstance.isLoop = false
        }
        Global.sharedInstance.defaults.set(Global.sharedInstance.isLoop, forKey: "isLoop")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
