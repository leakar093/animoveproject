//
//  SmsEnterViewController.swift
//  AnimooveProject
//
//  Created by User on 09/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class SmsEnterViewController: UIViewController {

    @IBOutlet weak var lblPhone: UILabel!
    var generic:Generic = Generic()

    @IBOutlet weak var txtSmsKod: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
     let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SmsEnterViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)

        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        txtSmsKod.text = ""
        // Hide the navigation bar on the this view controller
        lblPhone.text = Global.sharedInstance.phone
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
//        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnContinue(_ sender: UIButton) {
        var x = 0
        if x == 0{
            x = 1
        if txtSmsKod.text != ""{
        if Int(txtSmsKod.text!)! == Global.sharedInstance.smsKod {

            createAnonimyUser()
        }
        else{//הקש שוב מס' טלפון
//    Alert.sharedInstance.showAlerMess(mess: "הקוד שהוקש שגוי", vc: self);
// self.navigationController?.popViewController(animated: true)
            showAlerMess(mess: "הקוד שגוי", vc: self)
            }
        }
        else{
Alert.sharedInstance.showAlerMess(mess: "  חובה להקיש קוד",vc: self)
        }
        }
        
    }
    
    func showAlerMess(mess:String,vc:UIViewController)
    {
        let alertController = UIAlertController(title: "", message:
            mess, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default,handler: {
            _ in
           self.navigationController?.popViewController(animated: true)
        }))
        vc.present(alertController, animated: true, completion: nil)
        
        //  alertController.setValue(attributedString, forKey: "attributedMessage")
        //alertController.message = mess
        
    }
    
    func createAnonimyUser(){
        var dic = Dictionary<String,AnyObject>()
        
        dic["nvPhone"] = lblPhone.text as AnyObject
        //        dic[“iContractReportId”] = 31 as AnyObject
        
        generic.showNativeActivityIndicator(cont: self)
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
      let id = dic["Result"] as! Array<Int>
                        
                        Global.sharedInstance.userAnonime = User(id: (id[0]))
                        let RegistrationViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
                        self.generic.hideNativeActivityIndicator(cont: self)
                        self.navigationController?.pushViewController(RegistrationViewController, animated: true)
                        
                        Global.sharedInstance.defaults.setValue("yes", forKey: "isEnter")
                        Global.sharedInstance.defaults.setValue( Global.sharedInstance.userAnonime.getDic(), forKey: "oUser")
                    }
                    else{
                        if error["iErrorCode"] as? Int == 2601{
                            print(dic["Error"])
                             self.generic.hideNativeActivityIndicator(cont: self)
                            Alert.sharedInstance.showAlerMess(mess: "המס' קים במערכת", vc: self)
                        }
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
             self.generic.hideNativeActivityIndicator(cont: self)
        }, funcName: api.sharedInstance.CreateAnonymousUser)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
