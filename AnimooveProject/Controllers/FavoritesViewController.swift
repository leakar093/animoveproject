//
//  FavoritesViewController.swift
//  AnimooveProject
//
//  Created by User on 21/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var arrayFavoritesAnimoove:Array<Animoove> = []

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Global.sharedInstance.FavoritesAnimooves.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "VideoTableViewCell")as!VideoTableViewCell
        cell.selectionStyle = .none
       
        cell.setDisplayData(ani: Global.sharedInstance.FavoritesAnimooves[indexPath.row],index: indexPath.row, isVisible: true)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
  
        return tblFavorites.bounds.size.height /
        1.6
        //    return self.tblVideos.bounds.height / 1.5
        
    }

    @IBOutlet weak var tblFavorites: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
getMooviesFavorites()
       // TittleHeader(st: "Favorites")
        self.navigationItem.title = "Favorites";
 self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMooviesFavorites()
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = Global.sharedInstance.userRegister.iUserId as AnyObject
      
        
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        
                        
                        Global.sharedInstance.FavoritesAnimooves = Animoove().animooveToArrayGet(arrDic: dic["Result"] as! Array<Dictionary<String,AnyObject>>)
                        self.tblFavorites.reloadData()
                        //   self.updateTblHeight()
                        //self.tblMoovies.reloadData()
                    }
                    else{
                        print(dic["Error"])
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetFavoriteAnimoovesList)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
