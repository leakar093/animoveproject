//
//  RegistrationViewController.swift
//  AnimooveProject
//
//  Created by Studio on 14/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
import FBSDKLoginKit
class RegistrationViewController: UIViewController , FBSDKLoginButtonDelegate{

    
    @IBOutlet weak var btnFaceBook: FBSDKLoginButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-register.png")!)


        
        if (FBSDKAccessToken.current() == nil)
        {
            print("Not logged in..")
        }
        else
        {
            print("Logged in..")
        }
        
        btnFaceBook.readPermissions = ["public_profile", "user_friends","email"]
      
        btnFaceBook.delegate = self
        
        btnFaceBook.backgroundColor = UIColor.clear
        btnFaceBook.setImage(nil, for: UIControlState.normal)
        btnFaceBook.setTitleColor(UIColor.clear, for: .normal)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Normal)
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Selected)
        // Do any additional setup after loading the view.
        btnFaceBook.setBackgroundImage(UIImage(named: "login-with-facebook.png"), for: .normal)
        btnFaceBook.setBackgroundImage(UIImage(named: "login-with-facebook.png"), for: .selected)
    }
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func RegisterLater(_ sender: UIButton) {
        
//        let HomeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//
//
//        self.navigationController?.setViewControllers([HomeViewController], animated: true)
        let homeController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let myNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "myNav") as! UINavigationController
        myNav.setViewControllers([homeController], animated: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate; appDelegate.window?.rootViewController = myNav
    }
    // MARK: - login in facebook
    //====================login in facebook=======================
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        if error == nil
        {
            
            print("Login complete.")
            
            if(FBSDKAccessToken.current() != nil) {
                returnUserData()
            }
        }
        else
        {
            print(error.localizedDescription)
        }
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Normal)
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Selected)
        btnFaceBook.setBackgroundImage(UIImage(named: "login-with-facebook.png"), for: .normal)
        btnFaceBook.setBackgroundImage(UIImage(named: "login-with-facebook.png"), for: .selected)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!)
    {
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Normal)
//        print("User logged out...")
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Normal)
//        btnFaceBook.setBackgroundImage(UIImage(named: "group_2f.png"), forState: .Selected)
        
    }
    
    func returnUserData()
    {
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields" : "email, name, id, gender"])
        .start(completionHandler:  { (connection, result, error) in
            let result = result as? NSDictionary
            let email = result!["email"] as? String
            let user_name = result!["name"] as? String
            _ = result!["gender"] as? String
            _ = result!["id"]  as? String
                
            var user:User = User(_iUserId: Global.sharedInstance.userAnonime.iUserId, _iUserType: 2, _iUserStatusType: 1, _nvUserName: user_name!, _nvPassword: "", _iFBId: 0, _nvMail: email!, _nvPhone: "", _iAudioType: 1, _bReceiveWarnings: false, _bRememberMe: false)
           self.register(user: user)
        })
        
        
       
        
    }
    
    @IBOutlet weak var txtMail: UITextField!
    @IBAction func txtMail(_ sender: UITextField) {
        
      
    }
    @IBAction func btnSubmit(_ sender: UIButton) {
        let user:User = User(_iUserId: Global.sharedInstance.userAnonime.iUserId, _iUserType: 2, _iUserStatusType: 1, _nvUserName: "", _nvPassword: "", _iFBId: 0, _nvMail: txtMail.text!, _nvPhone: Global.sharedInstance.phone, _iAudioType: 1, _bReceiveWarnings: false, _bRememberMe: false)
        self.register(user: user)
    }
    func register(user:User){
        var dic = Dictionary<String,AnyObject>()
        
        //        dic[“iUserId”] = 1 as AnyObject
        //        dic[“iContractReportId”] = 31 as AnyObject
        dic["oUser"] = user.getDic() as AnyObject
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        Global.sharedInstance.userRegister = Global.sharedInstance.userAnonime
                        let homeController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        let myNav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "myNav") as! UINavigationController
                        myNav.setViewControllers([homeController], animated: true)
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate; appDelegate.window?.rootViewController = myNav
Global.sharedInstance.defaults.setValue(Global.sharedInstance.userRegister.getDic(), forKey: "oUser")
                    }
                    else{
                       // print(dic["Error"])
                    Alert.sharedInstance.showAlerMess(mess: "ההרשמה לא בוצעה כראוי", vc: self)
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            Alert.sharedInstance.showAlerMess(mess: "תקלת שרת", vc: self)
        }, funcName: api.sharedInstance.RegisterUser)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
