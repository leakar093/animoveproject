//
//  tableContainerViewController.swift
//  AnimooveProject
//
//  Created by Studio on 23/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
protocol controllerToPushDelegate {
    func controllerToPush(view:UIViewController,cat:Category,FromKey:Int)
}
class tableContainerViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    var generic:Generic = Generic()
    
    var viewCon:SortAnimooveByImugiesViewController?
    
    @IBOutlet weak var collImugies: UICollectionView!
    var indexP:IndexPath?
    var delegate:controllerToPushDelegate!=nil
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return  40
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if             Global.sharedInstance.tagOrImugi == 0{

        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier:"ImugiCollectionViewCell" , for: indexPath)as!ImugiCollectionViewCell
        //cell.delegate = self
        //cell.setDisplayData(cat: arrCategories[indexPath.row])
        if Global.sharedInstance.arrayImugies.count > indexPath.row{
        cell.setDisplayData(img: Global.sharedInstance.arrayImugies[indexPath.row])
        }else{
             cell.setDisplayData(img: Global.sharedInstance.arrayImugies[0])
        }
        return cell
        }
          let  cell = collectionView.dequeueReusableCell(withReuseIdentifier:"TagCollectionViewCell" , for: indexPath)as!TagCollectionViewCell
        if (indexP != nil){
            if indexP?.row != indexPath.row || indexP?.section != indexPath.section{
              cell.lblText.textColor = Colors.sharedInstance.blueKeybourd
                cell.viColor.backgroundColor = UIColor.clear
            }
            else{
                cell.lblText.textColor = Colors.sharedInstance.whiteBackGround
                cell.viColor.backgroundColor =
                Colors.sharedInstance.pinkKeybourd
            }
        }
        else{
            cell.lblText.textColor = Colors.sharedInstance.blueKeybourd
            cell.viColor.backgroundColor = UIColor.clear
        }
         if Global.sharedInstance.arrayImugies.count > indexPath.row{
        cell.setDisplayData(cat: Global.sharedInstance.arrCategories[indexPath.row])
        }
         else{
               cell.setDisplayData(cat: Global.sharedInstance.arrCategories[0])
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if             Global.sharedInstance.tagOrImugi == 1{
             (collImugies.cellForItem(at: indexPath)as!TagCollectionViewCell).viColor.backgroundColor = Colors.sharedInstance.pinkKeybourd
            (collImugies.cellForItem(at: indexPath)as!TagCollectionViewCell).lblText.textColor = Colors.sharedInstance.whiteBackGround
            indexP = indexPath
            collImugies.reloadData()
      
      
        }
         getMooviesByCategory(categoryID: Global.sharedInstance.arrCategories[indexPath.row])
        
    }
    func getMooviesByCategory(categoryID:Category)
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = Global.sharedInstance.userRegister.iUserId as AnyObject
        dic["iCategoryId"] = categoryID.iCategoryId as AnyObject
        
        generic.showNativeActivityIndicator(cont: self)
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        
                        
                        Global.sharedInstance.arrayAnimooveByImugies = Animoove().animooveToArrayGet(arrDic: dic["Result"] as! Array<Dictionary<String,AnyObject>>)
                        //   self.updateTblHeight()
//                        self.tblMoovies.reloadData()
        self.delegate.controllerToPush(view: self.viewCon!,cat: categoryID, FromKey: 0)
        self.generic.hideNativeActivityIndicator(cont: self)
                    }
                    else{
                        print(dic["Error"])
                        self.generic.hideNativeActivityIndicator(cont: self)
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            self.generic.hideNativeActivityIndicator(cont: self)
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetAnimoovesListByCategory)
    }
    
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if Global.sharedInstance.tagOrImugi == 0{
        let itemWidth = collectionView.bounds.width / 8
        let itemHeight = collectionView.bounds.height / 4
        return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemWidth = collectionView.bounds.width / 2
        let itemHeight = collectionView.bounds.height / 5
        return CGSize(width: itemWidth, height: itemHeight)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    viewCon = storyboard?.instantiateViewController(withIdentifier: "SortAnimooveByImugiesViewController")as! SortAnimooveByImugiesViewController
        collImugies.reloadData()
      
        // Do any additional setup after loading the view.
    }
//    override func view(_ animated: Bool) {
//        collImugies.reloadData()
//    }
    override func viewWillLayoutSubviews() {
        indexP = nil
        collImugies.reloadData()
          delegate = Global.sharedInstance.mainPage
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
