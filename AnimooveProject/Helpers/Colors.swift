//
//  Colors.swift
//  AnimooveProject
//
//  Created by User on 07/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class Colors: NSObject {
    override init() {
        
    }
    
    static let sharedInstance : Colors = {
        let instance = Colors()
        return instance
    }()
    
    var blueNavigation:UIColor = UIColor(red: 40/255.0, green: 108/255.0, blue: 192/255.0, alpha: 1.0)
    var grayColor:UIColor = UIColor(red: 154/255.0, green: 154/255.0, blue: 154/255.0, alpha: 1.0)
    var pinkColor:UIColor = UIColor(red: 196/255.0, green: 0/255.0, blue: 117/255.0, alpha: 1.0)
    var pinkNavigation:UIColor = UIColor(red: 196/255.0, green: 0/255.0, blue: 127/255.0, alpha: 1.0)
    var blueKeybourd:UIColor = UIColor(red: 21/255.0, green: 123/255.0, blue: 249/255.0, alpha: 1.0)
    var grayKeyboard:UIColor = UIColor(red: 191/255.0, green: 191/255.0, blue: 191/255.0, alpha: 1.0)
    var pinkKeybourd:UIColor = UIColor(red: 249/255.0, green: 100/255.0, blue: 192/255.0, alpha: 1.0)
    var whiteBackGround:UIColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1.0)
    var yellowColor:UIColor = UIColor(red: 255/255.0, green: 203/255.0, blue: 65/255.0, alpha: 1.0)
}
