//
//  Global.swift
//  bthree-ios
//
//  Created by User on 11.2.2016.
//  Copyright © 2016 Webit. All rights reserved.
//

import UIKit
import EventKit
import EventKitUI
import Contacts
import AddressBook

class Global: NSObject {
    override init() {
        
    }
    
    static let sharedInstance : Global = {
        let instance = Global()
        return instance
    }()
 
    var smsKod:Int = 0
    var phone:String = ""
    var userAnonime:User = User( )
    var idCategoryHome = 0
    let defaults = UserDefaults.standard
    var arrCategories:Array<Category> = []
    var userRegister:User = User()
    var FavoritesAnimooves:Array<Animoove> = []
    var arrayAnimooveByCategory:Array<Animoove> = []
    var menuBar:menuBarViewController?
    var arrayImugies:Array<UIImage> = []
    var tagOrImugi:Int = -1
    var keyboardEight = 0
    var ifReloadCategory:Bool = false
    var filteredSearch:Array<Category> = []
    var ifReload:Bool = false
    var mainPage:HomeViewController?
    var arrayAnimooveByImugies:Array<Animoove> = []
    var isMenuOpen:Bool = false
    var isLoop:Bool = true
    var isAutoPlay:Bool = true
//ממירה תמונה לסטרינג
//    func setImageToString(var image:UIImage)->String{
//        //var data:NSData
//        var imageData:NSString
//        var dataForJPEGFile:NSData
//        
//        if let _:UIImage=image{
//            dataForJPEGFile = UIImageJPEGRepresentation(image, 1.0)!
//            image=UIImage(data:dataForJPEGFile)!
//            let newSize:CGSize = CGSizeMake(image.size.width/2, image.size.height/2)
//            UIGraphicsBeginImageContext(newSize)
//            image.drawInRect(CGRectMake(0,0,newSize.width,newSize.height))
//            let newImage:UIImage=UIGraphicsGetImageFromCurrentImageContext()!
//            UIGraphicsEndImageContext()
//            //data=UIImagePNGRepresentation(newImage)!
//            dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.2)! as NSData//picks down
//            imageData = dataForJPEGFile.base64EncodedStringWithOptions(NSData.Base64EncodingOptions(rawValue: 0))
//            return imageData as String
//        }
//        else{
//            return ""
//        }
//    }
//   

    func loadImugies(){
    
        for item in Global.sharedInstance.arrCategories{
            if item.nvImogyFileData != ""{
                
                let url = URL(string: item.nvImogyFileData)
                
                let data = try? Data(contentsOf: url!)
                if data != nil{
                Global.sharedInstance.arrayImugies.append(UIImage(data: data!)!)
                }
            }
        }
    }
    ////////////////////
    //parse
    func parseJsonToInt(intToParse:AnyObject)->Int
    {
        let c = intToParse
        let int:String = (c.description)!
        if let checkIfParse = Int(int)
        {
            return checkIfParse
        }
        return 0
    }
    func parseJsonToFloat(floatToParse:AnyObject)->Float
    {
        let num = floatToParse
        let floatNum:String = (num.description)
        if (floatNum as NSString).floatValue != 0
        {
            return (floatNum as NSString).floatValue
        }
        return 0
        
//        if (floatToParse.text) != nil
//        {
//        if (floatToParse.text as NSString).floatValue != 0
//        {
//            return (floatToParse.text as NSString).floatValue
//        }
//        }
//        return 0
    }
    
    
    
    func parseJsonToString (stringToParse:AnyObject)-> String
    {
        //   if (stringToParse.isKindOfClass(NSNull.cl))
        let c:String = stringToParse.description
        if c == "<null>"
        {
            return ""
        }
        else if let checkIfParse:String = c
        {
            return checkIfParse
        }
        // if let string
        // {
        // return string
        //}
        return ""
    }
    

    

    
   

    
}



