//
//  Extentions.swift
//  LIA
//
//  Created by Lior Ronen on 17/09/2017.
//  Copyright © 2017 Webit. All rights reserved.
//

import UIKit

//class Extentions: NSObject {
//
//}
extension UIImageView{
    
    func setImageFromURl(stringImageUrl url: String){
        
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: data as Data)
            }
        }
    }
    // download image by url
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    

}
extension UIView {
    
    func dropShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
    }
}
extension UIColor{
    
    static let darkBlue = #colorLiteral(red: 0.09019607843, green: 0.1529411765, blue: 0.3058823529, alpha: 1)
    static let blue = #colorLiteral(red: 0.1411764706, green: 0.2156862745, blue: 0.3960784314, alpha: 1)
    static let yellowOrange = #colorLiteral(red: 0.9568627451, green: 0.7568627451, blue: 0.1019607843, alpha: 1)
    static let lightBlue = #colorLiteral(red: 0.2666666667, green: 0.7450980392, blue: 0.8470588235, alpha: 1)
    static let orange = #colorLiteral(red: 0.9529411765, green: 0.631372549, blue: 0.1176470588, alpha: 1)
    static let green = #colorLiteral(red: 0.3176470588, green: 0.662745098, blue: 0.2784313725, alpha: 1)
    static let pink = #colorLiteral(red: 0.8823529412, green: 0.07058823529, blue: 0.4392156863, alpha: 1)
    static let brown = #colorLiteral(red: 0.7490196078, green: 0.3215686275, blue: 0.1647058824, alpha: 1)
    static let red = #colorLiteral(red: 0.8666666667, green: 0.1294117647, blue: 0.1490196078, alpha: 1)
    static let darkGray = #colorLiteral(red: 0.4200314879, green: 0.4204269648, blue: 0.4321526885, alpha: 1)
    static let lightGray = #colorLiteral(red: 0.9562539458, green: 0.9563749433, blue: 0.9594156146, alpha: 1)
    static let Bordeaux = #colorLiteral(red: 0.7450980392, green: 0.1176470588, blue: 0.1764705882, alpha: 1)
    static let lightRed = #colorLiteral(red: 0.9254901961, green: 0.1098039216, blue: 0.1411764706, alpha: 1)
    static let lightOrange = #colorLiteral(red: 0.9568627451, green: 0.7568627451, blue: 0.1019607843, alpha: 1)
    static let darkDarkGray = #colorLiteral(red: 0.003921568627, green: 0.003921568627, blue: 0.003921568627, alpha: 0.9)
    static let gray = #colorLiteral(red: 0.3450980392, green: 0.3450980392, blue: 0.3568627451, alpha: 1)
    
    static let appGreen = #colorLiteral(red: 0.4941176471, green: 0.7568627451, blue: 0.2549019608, alpha: 1)
    static let appPink = #colorLiteral(red: 0.8823529412, green: 0.137254902, blue: 0.4039215686, alpha: 1)
    static let appLightBlue = #colorLiteral(red: 0.2666666667, green: 0.7450980392, blue: 0.8470588235, alpha: 1)
    static let appOrange = #colorLiteral(red: 0.9568627451, green: 0.7568627451, blue: 0.1019607843, alpha: 1)
}

extension UILabel{
    func circleLabel(){
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
           }
}
extension UIButton{
    func circleButton(){
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
}
extension UIView{
    func circleView(){
        self.layer.cornerRadius = self.frame.width/2
        self.layer.masksToBounds = true
    }
}
extension String{
    func getIsValidDate()->Bool{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if formatter.date(from: self) != nil {
            return true
        }
        return false
        }
    
    func getDate()->Date{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.date(from: self)!
    }
}
extension Date{
    
    func getString()->String{
        var dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
        }
    
}
