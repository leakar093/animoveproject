//
//  Category.swift
//  AnimooveProject
//
//  Created by User on 08/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class Category: NSObject {
    
  
    
    var iCategoryId:Int = 0
    var nvName:String = ""
    var nvImogyPath:String = ""
    var nvImogyFileData:String = ""
    var nvImgFileData:String = ""
    
    override init() {
        iCategoryId = 0
        nvName = ""
        nvImogyPath = ""
      nvImogyFileData = ""
        nvImgFileData = ""
    }
    
    init(_iCategoryId:Int,_nvName:String,_nvImogyPath:String,_nvImogyFileData:String,_nvImgFileData:String) {
        iCategoryId = _iCategoryId
        nvName = _nvName
        nvImogyPath = _nvImogyPath
      nvImogyFileData = _nvImogyFileData
        nvImgFileData = _nvImgFileData
    }
    
 func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iCategoryId"] = iCategoryId as AnyObject
        dic["nvName"] = nvName as AnyObject
        dic["nvImogyPath"] = nvImogyPath as AnyObject
       dic["nvImogyFileData"] = nvImogyFileData as AnyObject
         dic["nvImgFileData"] = nvImgFileData as AnyObject
        return dic
    }

    
    //לשים לב לפני השימוש בפונקציה שהשמשתנים שחוזרים מהשרת הם בדיוק באותם שמות
    func getCategoryFromDic(dic:Dictionary<String,AnyObject>) -> Category{
        let category:Category = Category()
        category.iCategoryId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iCategoryId"]!)
        category.nvName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvName"]!)
 
        category.nvImogyPath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImogyPath"]!)
        category.nvImogyFileData = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImogyFileData"]!)
        category.nvImgFileData = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImgFileData"]!)
        return category
        
    }
    
    
    
    func categoriesToArrayGet(arrDic:Array<Dictionary<String,AnyObject>>)->Array<Category>{
        var arrayCategories:Array<Category> = []
        var category:Category = Category()

        for i in 0  ..< arrDic.count
        {
            category = getCategoryFromDic(dic: arrDic[i])
            if category.nvName == "homePage"
            {
                Global.sharedInstance.idCategoryHome = category.iCategoryId
            }
            arrayCategories.append(category)


        }
        return arrayCategories
    }
    
    
}
