//
//  Animoove.swift
//  AnimooveProject
//
//  Created by User on 09/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class Animoove: NSObject {
    var   iAniMovieId:Int = 0
    var  iAdminStatus:Int = 0
    var   iStatus:Int = 0
    var   nvAnimooveStatus:String = ""
    var   nvName:String = ""
    var  iAniMatorId:Int = 0
    var   iAmountShared:Int = 0
    var  iViews:Int = 0
    var  bSelfImage:Bool = false
    var bIconCamera:Bool = false
    var nPrice:Float = 0
    var nPriceDeducted:Float = 0
    var nvURL:String = ""
    var nvTeaser:String = ""
    var bShareFb:Bool = false
    var iCategoryId:Int = 0
    var iIndexAnimovieForCategory:Int = 0
    var nvCategoryName:String = ""
    var nvImogyPath:String = ""
    var nvAnimatorName:String = ""
    var nvAnimatorImgPath:String = ""
    var nvAnimatorURL:String = ""
    var  nSerieAnimoviePrice:Float = 0
    var  nOrderBy:Float = 0
    var   iSerieId:Int = 0
    var  nvSerieName:String = ""
    var  nSeriePrice:Float = 0
    var  nSeriePriceDeducted:Float = 0
    var  oUserAnimoove:UserAnimoove = UserAnimoove()
    var loCategories:Array<Category> = []
    override init() {
        iAniMovieId = 0
       iAdminStatus = 0
           iStatus = 0
           nvAnimooveStatus = ""
           nvName = ""
          iAniMatorId = 0
           iAmountShared = 0
          iViews = 0
          bSelfImage = false
         bIconCamera = false
         nPrice = 0
         nPriceDeducted = 0
          nvURL = ""
         nvTeaser = ""
         bShareFb = false
         iCategoryId = 0
         iIndexAnimovieForCategory = 0
         nvCategoryName = ""
         nvImogyPath = ""
         nvAnimatorName = ""
         nvAnimatorImgPath = ""
         nvAnimatorURL = ""
          nSerieAnimoviePrice = 0
          nOrderBy = 0
           iSerieId = 0
          nvSerieName = ""
          nSeriePrice = 0
          nSeriePriceDeducted = 0
          oUserAnimoove = UserAnimoove()
         loCategories = []
        
    }
    
    
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iAniMovieId"] = iAniMovieId as AnyObject
        dic["iAdminStatus"] = iAdminStatus as AnyObject
        dic["iStatus"] = iStatus as AnyObject
         dic["nvAnimooveStatus"] = nvAnimooveStatus as AnyObject
         dic["nvName"] = nvName as AnyObject
         dic["iAniMatorId"] = iAniMatorId as AnyObject
         dic["iAmountShared"] = iAmountShared as AnyObject
         dic["bSelfImage"] = bSelfImage as AnyObject
         dic["bIconCamera"] = bIconCamera as AnyObject
         dic["nPrice"] = nPrice as AnyObject
        dic["nPriceDeducted"] = nPriceDeducted as AnyObject

        dic["nvURL"] = nvURL as AnyObject

        dic["nvTeaser"] = nvTeaser as AnyObject

        dic["bShareFb"] = bShareFb as AnyObject

        dic["iCategoryId"] = iCategoryId as AnyObject
        dic["iIndexAnimovieForCategory"] = iIndexAnimovieForCategory as AnyObject
        dic["nvCategoryName"] = nvCategoryName as AnyObject
        dic["nvImogyPath"] = nvImogyPath as AnyObject
        dic["nvAnimatorName"] = nvAnimatorName as AnyObject
        dic["nvAnimatorImgPath"] = nvAnimatorImgPath as AnyObject
        dic["nvAnimatorURL"] = nvAnimatorURL as AnyObject
        dic["nSerieAnimoviePrice"] = nSerieAnimoviePrice as AnyObject
        dic["nOrderBy"] = nOrderBy as AnyObject
        dic["iSerieId"] = iSerieId as AnyObject
        dic["nvSerieName"] = nvSerieName as AnyObject
        dic["nSeriePrice"] = nSeriePrice as AnyObject
        dic["nSeriePriceDeducted"] = nSeriePriceDeducted as AnyObject
         dic["oUserAnimoove"] = oUserAnimoove as AnyObject
         dic["loCategories"] = loCategories as AnyObject
        
        return dic
    }

    func getAnimooveFromDic(dic:Dictionary<String,AnyObject>) -> Animoove{
        let animoove:Animoove = Animoove()
        animoove.iAniMovieId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAniMovieId"]!)
        
         animoove.iAdminStatus = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAdminStatus"]!)
         animoove.iStatus = Global.sharedInstance.parseJsonToInt(intToParse: dic["iStatus"]!)
         animoove.iAniMovieId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAniMovieId"]!)
         animoove.iAniMatorId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAniMatorId"]!)
         animoove.iAmountShared = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAmountShared"]!)
         animoove.iViews = Global.sharedInstance.parseJsonToInt(intToParse: dic["iViews"]!)
        animoove.bSelfImage = dic["bSelfImage"]! as! Bool
         animoove.bIconCamera = dic["bIconCamera"]! as! Bool
        
        animoove.nvAnimooveStatus = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAnimooveStatus"]!)
        
        animoove.nvName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvName"]!)
        
      animoove.nPrice = dic["nPrice"]! as! Float
         animoove.nPriceDeducted = dic["nPriceDeducted"]! as! Float
        
        
        animoove.nvURL = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvURL"]!)
        
        animoove.nvTeaser = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvTeaser"]!)
        animoove.bShareFb = dic["bShareFb"]! as! Bool
           animoove.iAmountShared = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAmountShared"]!)
           animoove.iCategoryId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iCategoryId"]!)
            animoove.iIndexAnimovieForCategory = Global.sharedInstance.parseJsonToInt(intToParse: dic["iIndexAnimovieForCategory"]!)
        animoove.nvCategoryName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvCategoryName"]!)
        
        animoove.nvImogyPath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvImogyPath"]!)
        animoove.nvAnimatorName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAnimatorName"]!)
        animoove.nvAnimatorImgPath = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAnimatorImgPath"]!)
        animoove.nvAnimatorURL = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvAnimatorURL"]!)
            animoove.nSerieAnimoviePrice = dic["nSerieAnimoviePrice"]! as! Float
          animoove.nOrderBy = dic["nOrderBy"]! as! Float
          animoove.iSerieId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iSerieId"]!)
          animoove.nvSerieName = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvSerieName"]!)
         animoove.nSeriePrice = dic["nSeriePrice"]! as! Float
         animoove.nSeriePriceDeducted = dic["nSeriePriceDeducted"]! as! Float
        animoove.oUserAnimoove = UserAnimoove().getUserFromDic(dic: dic["oUserAnimoove"] as! Dictionary<String, AnyObject>)
        if dic["loCategories"] is NSNull{
       
        }
        else{
             animoove.loCategories = Category().categoriesToArrayGet(arrDic: dic["loCategories"] as! Array<Dictionary<String, AnyObject>>)
        }
        return animoove
        
    }
    
    func animooveToArrayGet(arrDic:Array<Dictionary<String,AnyObject>>)->Array<Animoove>{
        var arrayAnimooves:Array<Animoove> = []
        var animoove:Animoove = Animoove()
        
        for i in 0  ..< arrDic.count
        {
            animoove = getAnimooveFromDic(dic: arrDic[i])
           
            arrayAnimooves.append(animoove)
            
            
        }
        return arrayAnimooves
    }
    

}
