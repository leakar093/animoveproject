//
//  UserAnimoove.swift
//  AnimooveProject
//
//  Created by User on 09/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class UserAnimoove: NSObject {
    var iUserAniMovieId:Int = 0
    var iUserId:Int = 0
    var iAniMovieId :Int = 0
    var bFavorite:Bool = false
    var iStatusPayedType:Int = 0
    var  bShared:Bool = false
    override init() {
        
    }
    
    init(id:Int){
        iUserId = id
    }
    
    init(_iUserAniMovieId:Int,_iUserId:Int,_iAniMovieId:Int,_bFavorite:Bool,_iStatusPayedType:Int,_bShared:Bool) {
        iUserAniMovieId = _iUserAniMovieId
        iUserId = _iUserId
        iAniMovieId = _iAniMovieId
        bFavorite = _bFavorite
        iStatusPayedType = _iStatusPayedType
        bShared = _bShared
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserAniMovieId"] = iUserAniMovieId as AnyObject
        dic["iUserId"] = iUserId as AnyObject
        dic["iAniMovieId"] = iAniMovieId as AnyObject
        dic["bFavorite"] = bFavorite as AnyObject
        dic["iStatusPayedType"] = iStatusPayedType as AnyObject
        dic["bShared"] = bShared as AnyObject
        return dic
    }
    
    
    //לשים לב לפני השימוש בפונקציה שהשמשתנים שחוזרים מהשרת הם בדיוק באותם שמות
    func getUserFromDic(dic:Dictionary<String,AnyObject>) -> UserAnimoove{
        let user:UserAnimoove = UserAnimoove()
        user.iUserAniMovieId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserAniMovieId"]!)
           user.iUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserId"]!)
           user.iAniMovieId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAniMovieId"]!)
        user.bFavorite = dic["bFavorite"] as! Bool
       
            user.iStatusPayedType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iStatusPayedType"]!)
        
        user.bShared = dic["bShared"] as! Bool
        
      
        return user
        
    }
    
    
    
   
}
