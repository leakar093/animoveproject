//
//  User.swift
//  AnimooveProject
//
//  Created by Studio on 16/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class User: NSObject {
    var  iUserId:Int = 0
    var iUserType:Int  = 0
    var  iUserStatusType:Int = 0 
    var nvUserName:String = ""
    var nvPassword:String = ""
    var iFBId:Int = 0
    var nvMail:String = ""
    var nvPhone:String = ""
    var iAudioType:Int = 0
    var  bReceiveWarnings:Bool = false
    var  bRememberMe:Bool = false
    override init() {
        iUserId = 0
        iUserType = 0
        iUserStatusType = 0
        nvUserName = ""
        nvPassword = ""
        iFBId = 0
        nvMail = ""
        nvPhone = ""
        iAudioType = 0
        bReceiveWarnings = false
        bRememberMe = false
    }

    init(id:Int){
        iUserId = id
    }
  init(_iUserId:Int,_iUserType:Int,_iUserStatusType:Int,_nvUserName:String,_nvPassword:String,_iFBId:Int,_nvMail:String,_nvPhone:String,_iAudioType:Int,_bReceiveWarnings:Bool,_bRememberMe:Bool) {
        iUserId = _iUserId
        iUserType = _iUserType
        iUserStatusType = _iUserStatusType
        nvUserName = _nvUserName
        nvPassword = _nvPassword
        iFBId = _iFBId
        nvMail = _nvMail
        nvPhone = _nvPhone
        iAudioType = _iAudioType
        bReceiveWarnings = _bReceiveWarnings
        bRememberMe = _bRememberMe
    }
    
    func getDic()->Dictionary<String,AnyObject>
    {
        var dic:Dictionary<String,AnyObject> = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = iUserId as AnyObject
        dic["iUserType"] = iUserType as AnyObject
        dic["iUserStatusType"] = iUserStatusType as AnyObject
        dic["iAudioType"] = iAudioType as AnyObject
        dic["nvUserName"] = nvUserName as AnyObject
        dic["nvPassword"] = nvPassword as AnyObject
        dic["iFBId"] = iFBId as AnyObject
        dic["nvMail"] = nvMail as AnyObject
        dic["nvPhone"] = nvPhone as AnyObject
        dic["bReceiveWarnings"] = bReceiveWarnings as AnyObject
        dic["bRememberMe"] = bRememberMe as AnyObject
        return dic
    }
    
    
    //לשים לב לפני השימוש בפונקציה שהשמשתנים שחוזרים מהשרת הם בדיוק באותם שמות
    func getUserFromDic(dic:Dictionary<String,AnyObject>) -> User{
        let user:User = User()
        user.iUserId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserId"]!)
        user.iUserType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserType"]!)
        user.iUserStatusType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iUserStatusType"]!)
        user.bReceiveWarnings = dic["bReceiveWarnings"] as! Bool
        
        user.iFBId = Global.sharedInstance.parseJsonToInt(intToParse: dic["iFBId"]!)
         user.iAudioType = Global.sharedInstance.parseJsonToInt(intToParse: dic["iAudioType"]!)
        
        user.bRememberMe = dic["bRememberMe"] as! Bool
        
            user.nvMail = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvMail"]!)
            user.nvPhone = Global.sharedInstance.parseJsonToString(stringToParse: dic["nvPhone"]!)
        return user
        
    }
    
}
