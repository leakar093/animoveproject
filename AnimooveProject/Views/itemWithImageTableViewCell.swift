//
//  itemWithImageTableViewCell.swift
//  AnimooveProject
//
//  Created by User on 15/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class itemWithImageTableViewCell: UITableViewCell {

    @IBOutlet weak var lblImg: UILabel!
    @IBOutlet weak var viButtom: UIView!
    @IBOutlet weak var switchStatus: UISwitch!
    @IBOutlet weak var lblDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDisplayData(st:String,stImg:String){
        lblDesc.text = st
        lblImg.text = stImg
    }
}
