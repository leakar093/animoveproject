//
//  itemTableViewCell.swift
//  AnimooveProject
//
//  Created by User on 15/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class itemTableViewCell: UITableViewCell {

    @IBOutlet weak var lbldesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setDisplayData(st:String){
        lbldesc.text = st
    }
}
