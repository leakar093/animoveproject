//
//  VideoITemCollectionViewCell.swift
//  AnimooveProject
//
//  Created by Studio on 13/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
import AVKit
import Foundation
protocol presentDetailsDelegate {
    func presentDetails(ani:Animoove)
}
class VideoITemCollectionViewCell: UICollectionViewCell {
      var urlLoadVideos:String = "http://qa.webit-track.com/AnimooveWS/Movies/"
    @IBOutlet weak var viVideo: UIView!
    var urlVideo = ""
   
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var imgGifArrow: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var lblMusicMolti: UILabel!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var viText: UIView!
    @IBOutlet weak var viExt: UIView!
    var player:AVPlayer?
    var isPlayVideo:Bool = false
    func setDisplayData(ani:Animoove,isPlay:Bool){

        urlVideo =  ani.nvURL
        if Global.sharedInstance.defaults.value(forKey: "first") == nil{
        imgGifArrow.image = UIImage.gifImageWithName("arrow-left")
        }
        isPlayVideo = isPlay
    }
    
    @IBOutlet weak var imgArrow: UIImageView!
    //    override func layoutSubviews() {
//        <#code#>
//    }
//https://i.diawi.com/S21Uh9
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        lblMusicMolti.text = "\u{e923}"
      viVideo.layer.cornerRadius = 15
        viExt.layer.cornerRadius = 15
       // btnShare.layer.cornerRadius = 10
        if Global.sharedInstance.isAutoPlay == true{
            btnPlay.isHidden = true
           imgArrow.isHidden = true
        }
        else{
            btnPlay.isHidden = false
            imgArrow.isHidden = false

        }
        
        btnPlay.layer.cornerRadius = btnPlay.bounds.height / 2
        if urlVideo != ""{
            playVideo(from: urlVideo)
        }
    }
  
    
   
    @IBAction func btnPlayVideo(_ sender: UIButton) {
        if btnPlay.tag == 0{
        player?.play()
            isPlayVideo = true
            btnPlay.isHidden = true
            imgArrow.isHidden = true
            btnPlay.tag = 1}
        else{
            player?.pause()
            isPlayVideo = false
            btnPlay.tag = 0
            }
    }
    private func playVideo(from file:String) {
        let videoURL = URL(string: file)
        //player =  AVPlayer(url: videoURL!)
        let playerItem = AVPlayerItem(url: videoURL!)
        player = AVPlayer(playerItem: playerItem)
        let playerLayer = AVPlayerLayer(player: player)
    NotificationCenter.default.addObserver(self,selector: #selector(playerItemDidReachEnd(notification:)),name: Notification.Name.AVPlayerItemDidPlayToEndTime,object: player?.currentItem)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    self.viVideo.layer.addSublayer(playerLayer)
          playerLayer.frame = self.viVideo.layer.bounds
       viVideo.clipsToBounds = true
        if isPlayVideo == true && Global.sharedInstance.isAutoPlay == true{
            isPlayVideo = true
          player?.play()

        }
        else{
            
            isPlayVideo = false
            player?.pause()
       }
    }
    @objc func finishVideo()
    {
        print("Video Finished")
    
        playVideo(from: urlVideo)

    }
    
    @objc func playerItemDidReachEnd(notification: Notification) {
//        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
//            playerItem.seek(to: kCMTimeZero, completionHandler: nil)
//
//        }
         self.player?.seek(to: kCMTimeZero)
        
        if isPlayVideo == true && Global.sharedInstance.isLoop == true{
       
        self.player?.play()
        }
        if Global.sharedInstance.isAutoPlay == false && Global.sharedInstance.isLoop == false{
            btnPlay.isHidden = false
            imgArrow.isHidden = false
            btnPlay.tag = 0
        }
    }
    
    func playerDidFinishPlaying(note: NSNotification) {
        // Your code here
        if Global.sharedInstance.isLoop == true{
            playVideo(from: urlVideo)

        }
    }
    
}
