
//
//  searchTableViewCell.swift
//  AnimooveProject
//
//  Created by User on 26/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class searchTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var lblSearchText: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(cat:Category){
        lblSearchText.text = cat.nvName
    }
}
