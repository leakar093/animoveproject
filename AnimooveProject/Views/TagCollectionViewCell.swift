//
//  TagCollectionViewCell.swift
//  AnimooveProject
//
//  Created by User on 23/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var viColor: UIView!
    @IBOutlet weak var lblText: UILabel!
    func setDisplayData(cat:Category){
        lblText.text = cat.nvName
    }
}
