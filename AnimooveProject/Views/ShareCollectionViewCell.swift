//
//  ShareCollectionViewCell.swift
//  AnimooveProject
//
//  Created by Studio on 13/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
import Social
import FBSDKShareKit
import AddressBookUI
import MessageUI

class ShareCollectionViewCell: UICollectionViewCell ,FBSDKSharingDelegate,UIDocumentInteractionControllerDelegate,MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        Global.sharedInstance.mainPage?.dismiss(animated: true, completion: nil)
    }
    
    var ani:Animoove?
    
    @IBOutlet weak var btnSms: UIButton!
    @IBOutlet weak var btnWA: UIButton!
    
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var btnFB: UIButton!
    override func awakeFromNib() {
          btnFB.setTitle("\u{e908}", for: .normal)
        btnWA.setTitle("\u{e905}", for: .normal)
   btnSms.setTitle("\u{e912}", for: .normal) }
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print(results)

    }
    
    func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print("sharer NSError")
}
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        print("sharerDidCancel")

    }
    
    
    
    
    @IBAction func btnFaceBook(_ sender: UIButton) {
            if UIApplication.shared.canOpenURL(URL(string: "fb-messenger-api://")!) {
                let content = FBSDKShareLinkContent()
                
                content.contentURL = URL(string: (ani?.nvURL)!)//שליחת הלינק בהודעה שנשלחת בעת לחיצה עליה נפתחת באתר
               // let mapsURL = "\(self.link)"
                //            let message = "קיבלנו עליך המלצה חמה בין אם אתה עובד או בין העבודות זה הצעה ששוה לשמוע עליה.\n שלח קורות חייים ל:\n \(mapsURL)" as CFString
            //   let message = " " as CFString
                
                //let encodedString: String = String(CFURLCreateStringByAddingPercentEscapes(nil, message, nil, "!*'();:@&=+$,/?%#[]" as CFString, CFStringBuiltInEncodings.UTF8.rawValue))
            //    content.contentDescription = message as String;
                //       content.contentTitle = title
                //            content.contentURL = NSURL(string: url)
                //            content.imageURL = NSURL(string: self.appIconUrl)
                FBSDKMessageDialog.show(with: content, delegate: self)
               
            } else {
                UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/pl/app/messenger/id454638411?mt=8")!)
            }
        }
  
    @IBAction func btnWA(_ sender: UIButton) {
//        let mapsURL = ani?.nvURL
//        let message = mapsURL! as CFString
//        let encodedString: String = String(CFURLCreateStringByAddingPercentEscapes(nil, message, nil, "!*'();:@&=+$,/?%#[]" as CFString, CFStringBuiltInEncodings.UTF8.rawValue))
//        let url  =
//            URL(string: "whatsapp://send?text=\(encodedString)")
//        //Text which will be shared on WhatsApp is: "Hello Friends, Sharing some data here... !"
//
//        if UIApplication.shared.canOpenURL(url!) {
//            UIApplication.shared.openURL(url!)
//
//
//        }
//        let someText:String = "Hello want to share text also"
//        let objectsToShare:URL = URL(string: (ani?.nvURL)!)!
//        let sharedObjects:[AnyObject] = [objectsToShare as AnyObject,someText as AnyObject]
//        let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
//        activityViewController.popoverPresentationController?.sourceView = Global.sharedInstance.mainPage?.view
//
//        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook,UIActivityType.postToTwitter,UIActivityType.mail]
//
//        Global.sharedInstance.mainPage?.present(activityViewController, animated: true, completion: nil)
//        var documentInteractionController: UIDocumentInteractionController = UIDocumentInteractionController()
//        let urlWhats = "whatsapp://app"
//        if let urlString = urlWhats.removingPercentEncoding {
//            if let whatsappURL = NSURL(string: urlString) {
//
//                if UIApplication.shared.canOpenURL(whatsappURL as URL) {
//
//                    if let image = UIImage(named: "logo (1).png") {
//                        if let imageData = UIImageJPEGRepresentation(image, 1.0) {
//                            let tempFile = NSURL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("Documents/whatsAppTmp.wai")
//                            do {
//                                try imageData.write(to: tempFile!, options: .atomic)
//                                documentInteractionController = UIDocumentInteractionController(url: tempFile!)
//                   documentInteractionController.uti = "net.whatsapp.image"
//
//                    documentInteractionController.presentOpenInMenu(from: CGRect.zero, in: (Global.sharedInstance.mainPage?.view)!, animated: true)
//                            } catch {
//                                print(error)
//                            }
//                        }
//                    }
//
//                } else {
//                    // Cannot open whatsapp
//                }
//            }
//        }
        
//        let image = UIImage(named: "logo (1).png") // replace that with your UIImage
//
//        let filename = "myimage.wai"
//        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, false)[0] as! NSString
//        let destinationPath = documentsPath.stringByAppendingString("/" + filename).stringByExpandingTildeInPath
//        UIImagePNGRepresentation(image).writeToFile(destinationPath, atomically: false)
//        let fileUrl = NSURL(fileURLWithPath: destinationPath)! as NSURL
//
//        documentController = UIDocumentInteractionController(URL: fileUrl)
//        documentController.delegate = self
//        documentController.UTI = "net.whatsapp.image"
//        documentController.presentOpenInMenuFromRect(CGRectZero, inView: self.view, animated: false)
    }
    
    
    @IBAction func btnSms(_ sender: UIButton) {

        var messageController = MFMessageComposeViewController()
        messageController.messageComposeDelegate = self
        //messageController.body = message
        var videoData:Data?
        if MFMessageComposeViewController.canSendAttachments() {
            print("Attachments Can Be Sent.")
            var filePath: String = (ani?.nvURL)!
           
             do {
                 videoData = try Data(contentsOf:URL(string: filePath)!)
            }
            catch _ {
                // Error handling
            }
       
            var didAttachVideo: Bool = messageController.addAttachmentData(videoData ?? Data(), typeIdentifier: "public.movie", filename: filePath)
            if didAttachVideo {
                print("Video Attached.")
            }
            else {
                print("Video Could Not Be Attached.")
            }
        }
        Global.sharedInstance.mainPage?.present(messageController, animated: true)
  
    }
    
    @IBAction func btnSwitterOpen(_ sender: UIButton) {
        var shareToTwitter : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        shareToTwitter.setInitialText("initial text")
        //shareToTwitter.addImage(UIImage(named: "32.png"))
        var urlPath = URL(string: (ani?.nvURL)!)
        shareToTwitter.add(URL(string: (ani?.nvURL)!))
        Global.sharedInstance.mainPage?.present(shareToTwitter, animated: true, completion: nil)
    }
    @IBAction func btnMail(_ sender: UIButton) {
        var phoneNumber = "1112223333"
        let viberScheme = "viber://"
        let tel = "tel"
        let chat = "chat"
        var action = "<user selection, chat or tel>"
        // this could be @"chat" or @"tel" depending on the choice of the user
        if UIApplication.shared.canOpenURL(URL(string: viberScheme)!) {
            // viber is installed
            var myString = ""
            if (action == tel) {
                myString = "\(tel):\(phoneNumber)"
            }
            else if (action == chat) {
                myString = "\(chat):\(phoneNumber)"
            }
            
            var myUrl = URL(string: viberScheme + (myString))
            if UIApplication.shared.canOpenURL(myUrl!) {
                UIApplication.shared.openURL(myUrl!)
            }
            else {
                // wrong parameters
            }
        }
        else {
            // viber is not installed
        }

//         var docController: UIDocumentInteractionController = UIDocumentInteractionController()
//
//        let fileUrl = NSURL(fileURLWithPath: (ani?.nvURL)!)
//
//        docController = UIDocumentInteractionController(url: fileUrl as URL)
//        docController.uti = "net.whatsapp.movie"
// docController.presentOpenInMenu(from: CGRect.zero, in: (Global.sharedInstance.mainPage?.view)!, animated: true)
////    let fileUrl = NSURL(fileURLWithPath: path)!
////        var docController: UIDocumentInteractionController = UIDocumentInteractionController()
////
////        docController = UIDocumentInteractionController(url: NSURL.fileURL(withPath: (ani?.nvURL)!))
////        docController.uti = "net.whatsapp.movie"
////        docController.presentOpenInMenu(from: CGRect.zero, in: (Global.sharedInstance.mainPage?.view)!, animated: true)
    }
}
