//
//  CategoryCollectionViewCell.swift
//  AnimooveProject
//
//  Created by User on 07/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viSelect: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    override func awakeFromNib() {
        viExt.layer.cornerRadius = 4
        viExt.layer.masksToBounds = true
        viExt.layer.borderColor = UIColor(red: 221/255.0, green: 221/255.0, blue: 221/255.0, alpha: 1.0).cgColor
        viExt.layer.borderWidth = 1
    }
    @IBOutlet weak var viExt: UIView!
    func setDisplayData(cat:Category){
        lblDesc.text = cat.nvName
   self.imgCategory.setImageFromURl(stringImageUrl: cat.nvImgFileData)
    }
}
