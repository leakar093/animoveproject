
//
//  CategoriesTableViewCell.swift
//  AnimooveProject
//
//  Created by Studio on 20/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
protocol reloadTblMooviesDelegate {
    func reloadTbl(categoryID:Int)
}
class CategoriesTableViewCell: UITableViewCell {
    var delegate:reloadTblMooviesDelegate!=nil
    @IBOutlet weak var collCategories: UICollectionView!
    var indexP:IndexPath!
    var generic:Generic = Generic()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collCategories.dataSource = self
        collCategories.delegate = self
        indexP = IndexPath(row: 0, section: 0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDisplayData(){
        collCategories.reloadData()
       // Global.sharedInstance.ifReloadCategory = true
    }

}
// MARK: - collectionView
extension CategoriesTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  Global.sharedInstance.arrCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let  cell = collectionView.dequeueReusableCell(withReuseIdentifier:"CategoryCollectionViewCell" , for: indexPath)as!CategoryCollectionViewCell
        cell.setDisplayData(cat: Global.sharedInstance.arrCategories[indexPath.row])
        if (indexP != nil){
        if indexP.row == indexPath.row{
            cell.viSelect.isHidden = false
        }
        else{
            cell.viSelect.isHidden = true
        }
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
     //   getMooviesByCategory(categoryID: Global.sharedInstance.arrCategories[indexPath.row].iCategoryId)
        (collCategories.cellForItem(at: indexPath) as! CategoryCollectionViewCell).viSelect.isHidden = false
        indexP = indexPath
        collCategories.reloadData()

        delegate.reloadTbl(categoryID: Global.sharedInstance.arrCategories[indexPath.row].iCategoryId)
    }
    
    func getMooviesByCategory(categoryID:Int)
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = Global.sharedInstance.userRegister.iUserId as AnyObject
        dic["iCategoryId"] = categoryID as AnyObject
        
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        
                        
                        Global.sharedInstance.arrayAnimooveByCategory = Animoove().animooveToArrayGet(arrDic: dic["Result"] as! Array<Dictionary<String,AnyObject>>)
                        //   self.updateTblHeight()
                        //self.delegate.reloadTbl()
                        //self.tblMoovies.reloadData()
                    }
                    else{
                        print(dic["Error"])
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetAnimoovesListByCategory)
    }
    
}
extension CategoriesTableViewCell : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == Global.sharedInstance.arrCategories.count - 1{
            Global.sharedInstance.ifReloadCategory = true
        }
    }
}
extension CategoriesTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        if indexPath.row == 0
        //        {
        //        let itemsPerRow:CGFloat = 1
        //        let hardCodedPadding:CGFloat = 0
        let itemWidth = collectionView.bounds.width / 3.2
        let itemHeight = collectionView.bounds.height 
        return CGSize(width: itemWidth, height: itemHeight)
        //        }
        //        else
        //        {
        //            let itemsPerRow:CGFloat = 4.5
        //            // let hardCodedPadding:CGFloat = 0
        //            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
        //            let itemHeight = collectionView.bounds.height
        //            return CGSize(width: itemWidth + 10, height: itemHeight)
        //        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 3
    }
}

