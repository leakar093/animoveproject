//
//  ImugiCollectionViewCell.swift
//  AnimooveProject
//
//  Created by User on 23/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit

class ImugiCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgImugi: UIImageView!
    func setDisplayData(img:UIImage){
//        let url = URL(string: st)
//        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
        imgImugi.image = img
    }
}
