//
//  VideoTableViewCell.swift
//  AnimooveProject
//
//  Created by User on 08/11/2017.
//  Copyright © 2017 user. All rights reserved.
//

import UIKit
import AVKit
import Foundation
protocol showAlertDelegate {
    func showAlert()
}
class VideoTableViewCell: UITableViewCell {
    
    var delegate:presentDetailsDelegate!=nil
    var delegateAlert:showAlertDelegate!=nil
    var urlLoadVideos:String = "http://qa.webit-track.com/AnimooveWS/Movies/"
    @IBOutlet weak var viVideo: UIView!
    var isVisibleToPlay:Bool = false
    @IBOutlet weak var colltry: UICollectionView!
    
    @IBOutlet weak var lblTeaser: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnStar: UIButton!
    @IBOutlet weak var viText: UIView!
    @IBOutlet weak var collVideos: UICollectionView!
    var urlVideo:String = ""
    
    @IBOutlet weak var lblnumSee: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    var indexPathRow:Int = 0
    var ani:Animoove?
    var bFavorite:Bool = false
    var iStatusPayedType:Int = 0
    var bShared:Bool = false
    var index:IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collVideos.allowsSelection = true
        collVideos.allowsMultipleSelection = true
        collVideos.delegate = self
        collVideos.dataSource = self
        collVideos.transform = CGAffineTransform(scaleX: -1, y: 1)
        
        btnStar.setTitle("\u{e926}", for: .normal)
    }
    
    
    override func layoutSubviews() {
        //  playVideo(from: urlVideo)
        super.layoutSubviews()
        viText.layer.cornerRadius = 15
        btnShare.layer.cornerRadius = btnShare.frame.height/2
        btnShare.layer.masksToBounds = true
        let indexPath:IndexPath = NSIndexPath(item: 1, section: 0) as IndexPath
        // collVideos.scrollToItem(at: indexPath  , at:.right , animated: false)
        collVideos.reloadData()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
  
    func setDisplayData(ani:Animoove,index:Int,isVisible:Bool){
        //        playVideo(from: urlLoadVideos + ani.nvURL)\
        urlVideo = urlLoadVideos + ani.nvURL
        indexPathRow = index
        
        self.ani = ani
        lblPrice.text = "$" + String(Int(ani.nPrice))
        lblnumSee.text = String(Int(ani.iViews))
        lblTeaser.text = ani.nvTeaser
        if ani.oUserAnimoove.bFavorite == true{
            btnStar.setTitle("\u{e925}", for: .normal)
            btnStar.tag = 1
            btnStar.setTitleColor(Colors.sharedInstance.yellowColor, for: .normal)
        }
        else{
            btnStar.setTitle("\u{e926}", for: .normal)
            btnStar.setTitleColor(Colors.sharedInstance.grayColor, for: .normal)
            btnStar.tag = 0
        }
       isVisibleToPlay = isVisible
        collVideos.reloadData()
        
    }
    private func playVideo(from file:String) {
        
        let videoURL = URL(string: file)
        //        let player = AVPlayer(url: videoURL!)
        //        let playerViewController = AVPlayerViewController()
        //        playerViewController.player = player
        //        self.present(playerViewController, animated: true) {
        //            playerViewController.player!.play()
        //        }
        
        let player =  AVPlayer(url: videoURL!)
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.viVideo.bounds
        
        self.viVideo.layer.addSublayer(playerLayer)
        
        player.play()
    }
    
    @IBAction func openShare(_ sender: UIButton) {
        if btnShare.tag == 0{
            let index:IndexPath = IndexPath(row: 1, section: 0)
            collVideos.scrollToItem(at: index, at: .right, animated: true)
            btnShare.tag = 1
        }
        else{
            let index:IndexPath = IndexPath(row: 0, section: 0)
            collVideos.scrollToItem(at: index, at: .right, animated: true)
            btnShare.tag = 0
        }
    }
    
    @IBAction func btnStarChoose(_ sender: UIButton) {
        if btnStar.tag == 0{
            btnStar.setTitle("\u{e925}", for: .normal)
            btnStar.setTitleColor(Colors.sharedInstance.yellowColor, for: .normal)
            btnStar.tag = 1
            bFavorite = true
            self.ani?.oUserAnimoove.bFavorite = true
            
            UpdateUserAnimovie()
        }
        else{
            btnStar.setTitle("\u{e926}", for: .normal)
            btnStar.setTitleColor(Colors.sharedInstance.grayColor, for: .normal)
            btnStar.tag = 0
            bFavorite = false
            self.ani?.oUserAnimoove.bFavorite = false
            UpdateUserAnimovie()
            
        }
    }
    
    
    
    func UpdateUserAnimovie()
    {
        
        
        var dic = Dictionary<String,AnyObject>()
        dic["iUserId"] = Global.sharedInstance.userRegister.iUserId as AnyObject
        dic["iAniMovieId"] = self.ani?.iAniMovieId as AnyObject
        dic["bFavorite"] = bFavorite as AnyObject
        dic["iStatusPayedType"] = iStatusPayedType as AnyObject
        dic["bShared"] = bShared as AnyObject
        
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if let result = response as? Dictionary<String, AnyObject>{
                var dic:Dictionary<String, AnyObject> = result
                
                if let error = dic["Error"] as? NSDictionary
                {
                    if error["iErrorCode"] as? Int == 0 {
                        var val = dic["Result"] as! Array<Bool>
                        if val[0] == false{
                            self.delegateAlert.showAlert()
                            
                        }
                    }
                    else{
                        print(dic["Error"])
                    }
                }
                
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.UpdateUserAnimovie)
    }
    
    
}

// MARK: - collectionView
extension VideoTableViewCell : UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0
        { let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoITemCollectionViewCell", for: indexPath as IndexPath) as! VideoITemCollectionViewCell
            if self.ani != nil{
                cell.setDisplayData(ani: self.ani!, isPlay:isVisibleToPlay)
                
            }
            
            
            cell.transform = CGAffineTransform(scaleX: -1, y: 1)
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            //
            if index?.row == 0 && Global.sharedInstance.defaults.value(forKey: "first") == nil {
                cell.imgGifArrow.isHidden = false
                Global.sharedInstance.defaults.setValue("first" , forKey: "first")
                
            }
            else{
                if Global.sharedInstance.defaults.value(forKey: "first") == nil{
                    cell.imgGifArrow.isHidden = true}
            }
            return cell
        }
        let   cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShareCollectionViewCell", for: indexPath as IndexPath) as! ShareCollectionViewCell
        cell.ani = self.ani
        cell.transform = CGAffineTransform(scaleX: -1, y: 1)
        return cell
    }
   
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate.presentDetails(ani:self.ani!)
    }
}
extension VideoTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        if indexPath.row == 0
        //        {
        //        let itemsPerRow:CGFloat = 1
        //        let hardCodedPadding:CGFloat = 0
        if indexPath.row == 0{
            let itemWidth = collectionView.bounds.width
            let itemHeight = collectionView.bounds.height
            return CGSize(width: itemWidth, height: itemHeight)
        }
        let itemWidth = collectionView.bounds.width * 0.6
        let itemHeight = collectionView.bounds.height
        return CGSize(width: itemWidth, height: itemHeight)
        //        }
        //        else
        //        {
        //            let itemsPerRow:CGFloat = 4.5
        //            // let hardCodedPadding:CGFloat = 0
        //            let itemWidth = collectionView.bounds.width / itemsPerRow//) - hardCodedPadding
        //            let itemHeight = collectionView.bounds.height
        //            return CGSize(width: itemWidth + 10, height: itemHeight)
        //        }
    }
}

